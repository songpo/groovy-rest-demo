package com.liusongpo.demo.service.impl

import com.liusongpo.common.Page
import com.liusongpo.demo.model.UserInfo
import com.liusongpo.demo.repository.UserRepository
import com.liusongpo.demo.service.IUserService
import org.springframework.stereotype.Service

@Service
class UserServiceImpl implements IUserService {

    private UserRepository demoRepository

    UserServiceImpl(UserRepository demoRepository) {
        this.demoRepository = demoRepository
    }

    /**
     * 创建
     *
     * @param demo 参数
     * @return 创建结果
     */
    @Override
    UserInfo create(UserInfo demo) {
        return this.demoRepository.create(demo)
    }

    /**
     * 创建
     *
     * @param demo 参数
     * @return 创建结果
     */
    @Override
    UserInfo update(String id, UserInfo demo) {
        this.demoRepository.update id, demo
    }

    /**
     * 查询单个
     *
     * @param id 唯一标识
     * @return 查询结果
     */
    @Override
    UserInfo load(String id) {
        // TODO 能这么写，真是太爽了，注意不能加return关键字
        this.demoRepository.findOne id
    }

    /**
     * 查询单个
     *
     * @param id 唯一标识
     * @return 查询结果
     */
    @Override
    void delete(String id) {
        this.demoRepository.remove id
    }

    /**
     * 查询所有
     *
     * @param demo 查询条件
     * @return 查询结果
     */
    @Override
    List<UserInfo> findAll(UserInfo demo) {
        this.demoRepository.findAll demo
    }

    /**
     * 分页查询
     *
     * @param page 页码，从1开始
     * @param size 容量，大于0
     * @param demo 查询条件
     * @return 查询结果
     */
    @Override
    Page<UserInfo> findAll(Integer page, Integer size, UserInfo demo) {
        this.demoRepository.findAll page, size, demo
    }
}
