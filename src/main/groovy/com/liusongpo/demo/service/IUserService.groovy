package com.liusongpo.demo.service

import com.liusongpo.common.Page
import com.liusongpo.demo.model.UserInfo

interface IUserService {

    /**
     * 创建
     *
     * @param demo 参数
     * @return 创建结果
     */
    UserInfo create(UserInfo demo)

    /**
     * 更新
     *
     * @param id 唯一标识
     * @param demo 实体
     * @return 更新结果
     */
    UserInfo update(String id, UserInfo demo)

    /**
     * 查询单个
     *
     * @param id 唯一标识
     * @return 查询结果
     */
    UserInfo load(String id)

    /**
     * 根据唯一标识删除
     *
     * @param id 唯一标识
     * @return 查询结果
     */
    void delete(String id)

    /**
     * 查询所有
     *
     * @param demo 查询条件
     * @return 查询结果
     */
    List<UserInfo> findAll(UserInfo demo)

    /**
     * 分页查询
     *
     * @param page 页码，从1开始
     * @param size 容量，大于0
     * @param demo 查询条件
     * @return 查询结果
     */
    Page<UserInfo> findAll(Integer page, Integer size, UserInfo demo)
}