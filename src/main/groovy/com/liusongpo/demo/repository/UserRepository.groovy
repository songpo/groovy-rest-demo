package com.liusongpo.demo.repository

import com.liusongpo.common.Page
import com.liusongpo.demo.model.UserInfo
import org.springframework.stereotype.Component

import java.util.stream.Collectors

@Component
class UserRepository {

    private Map<String, UserInfo> demoMap = new TreeMap<>()

    UserRepository() {
        for (int i = 0; i < 1000; i++) {
//            Demo demo = new Demo()
//            demo.id = UUID.randomUUID().toString()
//            demo.username = "username${i}"
//            demo.password = "password"
//            demo.email = demo.username + '@liusongpo.com'

            // TODO 爽
            UserInfo demo = new UserInfo(
                    id: UUID.randomUUID().toString(),
                    username: "username${i}",
                    password: "password",
                    email: "username${i}" + '@liusongpo.com'
            )

            this.demoMap[demo.id] = demo
        }
    }

    /**
     * 新增
     * @param demo 实体
     * @return
     */
    UserInfo create(UserInfo demo) {
        demo.id = UUID.randomUUID().toString()
        this.demoMap[demo.id] = demo
    }

    /**
     * 更新
     * @param id 唯一标识
     * @param demo 实体
     * @return
     */
    UserInfo update(String id, UserInfo demo) {
        UserInfo item = this.demoMap[id]
        if (null != item) {
            item.password = demo.password
            item.email = demo.email
        }
        // TODO 最后一行作为返回值，可以忽略return关键字，爽
        item
    }

    /**
     * 删除
     *
     * @param id 唯一标识
     */
    void remove(String id) {
        this.demoMap.remove(id)
    }

    /**
     * 删除
     *
     * @param id 唯一标识
     */
    UserInfo findOne(String id) {
        this.demoMap[id]
    }

    /**
     * 查询所有
     *
     * @param demo 查询条件
     * @return 查询结果
     */
    List<UserInfo> findAll(UserInfo demo) {
        def stream = this.demoMap.values().parallelStream()
        if (demo.id != null && !demo.id.isBlank()) {
            stream = stream.filter(e -> e.id == demo.id)
        }
        if (demo.username != null && !demo.username.isBlank()) {
            stream = stream.filter(e -> e.username.startsWith(demo.username) || e.username.endsWith(username.email))
        }
        if (demo.email != null && !demo.email.isBlank()) {
            stream = stream.filter(e -> e.email.startsWith(demo.email) || e.email.endsWith(demo.email))
        }

        stream.collect(Collectors.toList())
    }

    /**
     * 分页查询
     *
     * @param page 页码，从1开始
     * @param size 容量，大于0
     * @param demo 查询条件
     * @return 查询结果
     */
    Page<UserInfo> findAll(Integer page, Integer size, UserInfo demo) {
        if (page >= 1) {
            page -= 1
        } else {
            page = 0
        }

        if (size == null || size < 0) {
            size = 10
        }

        def stream = this.demoMap.values().parallelStream()
        if (demo.id != null && !demo.id.isBlank()) {
            stream = stream.filter(e -> e.id == demo.id)
        }
        if (demo.username != null && !demo.username.isBlank()) {
            stream = stream.filter(e -> e.username.startsWith(demo.username) || e.username.endsWith(username.email))
        }
        if (demo.email != null && !demo.email.isBlank()) {
            stream = stream.filter(e -> e.email.startsWith(demo.email) || e.email.endsWith(demo.email))
        }

        List<UserInfo> list = stream
                .skip(page * size)
                .limit(size)
                .collect(Collectors.toList())

        Page<UserInfo> pageImpl = new Page<>();
        pageImpl.page = page + 1
        pageImpl.size = size
        pageImpl.list = list

        return pageImpl
    }
}
