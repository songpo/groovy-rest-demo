package com.liusongpo.demo.controller

import com.liusongpo.common.Page
import com.liusongpo.demo.model.UserInfo
import com.liusongpo.demo.service.IUserService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/users")
class UserController {

    private IUserService demoService;

    UserController(IUserService demoService) {
        this.demoService = demoService
    }

    @PostMapping
    UserInfo create(@RequestBody UserInfo demo) {
        return this.demoService.create(demo)
    }

    @GetMapping("/{id}")
    UserInfo load(@PathVariable String id) {
        return this.demoService.load(id)
    }

    @GetMapping("/list")
    List<UserInfo> findAll(UserInfo demo) {
        this.demoService.findAll demo
    }

    @GetMapping
    Page<UserInfo> findAll(Integer page, Integer size, UserInfo demo) {
        this.demoService.findAll page, size, demo
    }
}
